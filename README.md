Author: Vu Vo
ID: 951437454
email: vuv@uoregon.edu
# Project 5: Brevet time calculator with Ajax and MongoDB

Simple list of controle times from project 4 stored in MongoDB database.

To Run:
'docker-compose up' OR 'docker-compose up --build'

IMPORTANT NOTE:
When I ran docker-compose with the provided 'docker-compose.yml' file, I received the following error:
					web_1  | python: can't open file 'app.py': [Errno 2] No such file or directory
					dockermongo_web_1 exited with code 2
					
The way I fixed this is that I commented out the following line from docker-compose.yml:
		volume:
			-.:code					
And the containers compiled and worked as expected. However, I'm not sure if this fix will allow docker-compose to run 
in LINUX machines, it may not work. But I'm working on Docker Windows so this is all I can do given the time constraints.
If it doesn't work in the grader's machine, please let me know so I can show you in my machine because I need all the points I can get!


