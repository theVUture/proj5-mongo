import acp_times
import nose
import logging
import arrow

logging.basicConfig(format='%(levelname)s:%(message)s',level=logging.WARNING)
log = logging.getLogger(__name__)

date = arrow.Arrow(2013,7,5)
assert acp_times.open_time(130, 200, arrow.get(date)) == (date.shift(hours=4,minutes=25)).isoformat()
assert acp_times.close_time(130, 200, arrow.get(date)) == (date.shift(hours=10)).isoformat()
assert acp_times.open_time(600, 1005, arrow.get(date)) == (date.shift(hours=22,minutes=22)).isoformat()
assert acp_times.close_time(600, 1005, arrow.get(date)) == (date.shift(hours=48,minutes=45)).isoformat()
assert acp_times.open_time(550, 600, arrow.get(date)) == (date.shift(hours=17,minutes=8)).isoformat()
assert acp_times.close_time(550, 600, arrow.get(date)) == (date.shift(hours=36,minutes=40)).isoformat()
