import os
import flask
from flask import Flask, redirect, url_for, request, render_template
from pymongo import MongoClient
import arrow
import acp_times_solved
import config
import logging
import flask_brevets

app = Flask(__name__)

client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.tododb

@app.route('/')
@app.route("/index")
def todo():
    _items = db.tododb.find()
    items = [item for item in _items]

    return render_template('calc.html', items=items)

@app.route("/todo")
def display():
    _items = db.tododb.find()
    items = []
    for item in _items:
    	items.append(item)

    # items = [item for item in _items]

    return render_template('todo.html', items=items)

@app.route('/new', methods=['POST'])
def new():
    opening_stuff = request.form.getlist("open")
    closing_stuff = request.form.getlist("close")
    km_stuff = request.form.getlist("km")

    for i in range(len(opening_stuff)):
        item_doc = {
            'open_times': opening_stuff[i],
            'close_times': closing_stuff[i],
            'km': km_stuff[i]
        }
        db.tododb.insert_one(item_doc)

    return redirect(url_for('todo'))

@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    # get arguments
    km = request.args.get('km', 999, type=float)
    brevet_dist = request.args.get('bre_dist', 999, type=int)
    start_time = request.args.get('st_time', 999, type=str)
    start_date = request.args.get('st_day', 999, type=str)

    app.logger.debug("start time={}".format(start_time))
    app.logger.debug("start date={}".format(start_date))
    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))
    # FIXME: These probably aren't the right open and close times
    # and brevets may be longer than 200km
    # the time are fixed
    time = arrow.get("{}T{}".format(start_date, start_time))
    time = time.isoformat()

    open_time = acp_times_solved.open_time(km, brevet_dist, time)
    close_time = acp_times_solved.close_time(km, brevet_dist, time)
    result = {"open": open_time, "close": close_time}
    return flask.jsonify(result=result)

if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)
